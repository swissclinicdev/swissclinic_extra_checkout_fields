<?php

namespace Swissclinic\CheckoutExtraFields\Model;

class Config
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
    }

    const CHECKOUT_SWISSCLINIC_CHECKOUT_RG_FIELDS_ENABLED = 'checkout/swissclinic_checkout_extra_fields/enabled';

    /**
     * @param null $store
     * @return mixed
     */
    public function isEnabled($store = null)
    {
        return $this->scopeConfig->getValue(self::CHECKOUT_SWISSCLINIC_CHECKOUT_RG_FIELDS_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $store);
    }

}
