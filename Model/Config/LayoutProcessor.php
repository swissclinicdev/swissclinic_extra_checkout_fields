<?php

use Swissclinic\CheckoutExtraFields\Helper\Data as Helper;

class LayoutProcessor implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface
{

    protected $_helper;

    public function __construct(
        Helper $helperConfig
    )
    {
        $this->_helperConfig = $helperConfig;
    }

    public function process($jsLayout)
    {
        return $jsLayout;
    }
}

