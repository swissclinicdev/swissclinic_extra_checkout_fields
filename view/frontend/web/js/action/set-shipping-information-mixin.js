/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (setShippingInformationAction) {

        return wrapper.wrap(setShippingInformationAction, function (originalAction) {

            var shippingAddress = quote.shippingAddress();

            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }

            try {
                for (i = 0; i < shippingAddress.customAttributes.length; i++) {
                    if (shippingAddress.customAttributes[i].attribute_code === 'house_number') {
                        shippingAddress['extension_attributes']['house_number'] = shippingAddress.customAttributes[i].value;
                    }
                }

            } catch (e) {
                return originalAction();
            }

            //pass execution to original action ('Magento_Checkout/js/action/set-shipping-information')
            return originalAction();
        });
    };
});