define([
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function(wrapper, quote) {
    'use strict';

    return function (createShippingAddressAction) {
        return wrapper.wrap(createShippingAddressAction, function(originalAction, addressData) {

            var shippingAddress = quote.shippingAddress();

            if (addressData.custom_attributes === undefined) {
                return originalAction();
            }

            try {
                addressData.custom_attributes['house_number'] = shippingAddress.customAttributes['house_number'];
            } catch (e) {
                return originalAction();
            }

            console.log(addressData);
            console.log(shippingAddress);

            return originalAction();
        });
    };
});