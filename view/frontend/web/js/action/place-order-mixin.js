/*jshint browser:true jquery:true*/
/*global alert*/
define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper, quote) {
    'use strict';

    return function (placeOrderAction) {

        return wrapper.wrap(placeOrderAction, function (originalAction) {

            var billingAddress = quote.billingAddress();
            var shippingAddress = quote.shippingAddress();


            if(billingAddress.customAttributes === undefined) {
                billingAddress.customAttributes = {};
            }

            if(billingAddress['extension_attributes'] === undefined) {
                billingAddress['extension_attributes'] = {};
            }

            try {
                for (i = 0; i < billingAddress.customAttributes.length; i++) {
                    if (billingAddress.customAttributes[i].attribute_code === 'house_number') {
                        billingAddress['extension_attributes']['house_number'] = billingAddress.customAttributes[i].value;
                    }
                }

            } catch (e) {
                return originalAction();
            }

            console.log(shippingAddress);
            console.log(billingAddress);

            return originalAction();
        });
    };
});