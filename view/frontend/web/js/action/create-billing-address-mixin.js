define([
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function(wrapper) {
    'use strict';

    return function (createBillingAddressAction) {
        return wrapper.wrap(createBillingAddressAction, function(originalAction, addressData) {

            var billingAddress = quote.billingAddress();

            if (addressData.custom_attributes === undefined) {
                return originalAction();
            }

            try {
                addressData.custom_attributes['house_number'] = billingAddress.customAttributes['house_number'];
            } catch (e) {
                return originalAction();
            }

            console.log(shippingAddress);
            console.log(addressData);

            return originalAction();
        });
    };
});