/* 
* Adding this mixing due to: https://github.com/magento/magento2/commit/6d985af3486280a5a672ef32681a6cdb2a148aa3
* Should be removed if magento version is >= 2.3.5
* */
define([], function () {
    'use strict';

    return function (BillingAddress) {
        return BillingAddress.extend({
            getCustomAttributeLabel: function (attribute) {
                var resultAttribute;

                if (typeof attribute === 'string') {
                    return attribute;
                }

                if (attribute.label) {
                    return attribute.label;
                }

                if (typeof this.source.get('customAttributes') !== 'undefined') {
                    resultAttribute = _.findWhere(this.source.get('customAttributes')[attribute['attribute_code']], {
                        value: attribute.value
                    });
                }

                return resultAttribute && resultAttribute.label || attribute.value;
            }
        });
    }
});