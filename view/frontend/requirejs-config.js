var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/set-shipping-information': {
                'Swissclinic_CheckoutExtraFields/js/action/set-shipping-information-mixin': true
            },
            'Magento_Checkout/js/action/set-payment-information': {
                'Swissclinic_CheckoutExtraFields/js/action/set-payment-information-mixin': true
            },
            'Magento_Checkout/js/action/place-order': {
                'Swissclinic_CheckoutExtraFields/js/action/place-order-mixin': true
            },
            'Magento_Checkout/js/action/create-shipping-address': {
                'Swissclinic_CheckoutExtraFields/js/action/create-shipping-address-mixin': true
            },
            'Magento_Checkout/js/action/create-billing-address': {
                'Swissclinic_CheckoutExtraFields/js/action/create-billing-address-mixin': true
            },
            'Magento_Checkout/js/view/billing-address': {
                'Swissclinic_CheckoutExtraFields/js/view/billing-address-mixin': true
            }
        }
    }
};