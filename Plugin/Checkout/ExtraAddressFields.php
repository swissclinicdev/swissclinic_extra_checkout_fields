<?php

namespace Swissclinic\CheckoutExtraFields\Plugin\Checkout;

use Swissclinic\CheckoutExtraFields\Helper\Data as Helper;
use Magento\Checkout\Block\Checkout\LayoutProcessor;
use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Eav\Model\Config;


class ExtraAddressFields
{

    private $eavConfig;

    private $_helper;

    public function __construct(
        Config $config,
        Helper $helper
    )
    {
        $this->eavConfig = $config;
        $this->_helper = $helper;
    }

    public function afterProcess(LayoutProcessor $subject, $result)
    {

        $enabled = $this->_helper->isEnabled();

        if ($enabled) {

            $customAttribute = $this->eavConfig->getAttribute(
                AddressMetadataInterface::ENTITY_TYPE_ADDRESS,
                'house_number'
            );

            if (isset($customAttribute)) {
                $houseNumberComponent = [
                    'component' => 'Magento_Ui/js/form/element/abstract',
                    'config' => [
                        // customScope is used to group elements within a single form (e.g. they can be validated separately)
                        'customScope' => 'shippingAddress.custom_attributes',
                        'customEntry' => null,
                        'template' => 'ui/form/field',
                        'elementTmpl' => 'ui/form/element/input',
                        'tooltip' => false,
                        'label' => 'House Number'
                    ],
                    'dataScope' => 'shippingAddress.custom_attributes.house_number',
                    'label' => 'House Number',
                    'provider' => 'checkoutProvider',
                    'sortOrder' => "75",
                    'validation' => [
                        'required-entry' => true
                    ],
                    'options' => [],
                    'filterBy' => null,
                    'customEntry' => null,
                    'visible' => true,
                ];

                $result['components']['checkout']['children']['steps']['children']['shipping-step']['children']['shippingAddress']['children']['shipping-address-fieldset']['children']['house_number'] = $houseNumberComponent;
            }
        }

        return $result;
    }
}