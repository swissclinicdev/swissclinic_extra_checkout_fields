<?php

namespace Swissclinic\CheckoutExtraFields\Plugin\Checkout\Model;

use Swissclinic\CheckoutExtraFields\Helper\Data as Helper;

class PaymentInformationManagement
{
    private $_helper;

    public function __construct(
        Helper $helper
    )
    {
        $this->_helper = $helper;
    }

    public function beforeSavePaymentInformation(
        \Magento\Checkout\Model\PaymentInformationManagement $subject,
        $cartId,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $enabled = $this->_helper->isEnabled();

        if ($enabled) {
            if (!$billingAddress) {
                return;
            }

            $houseNumber = $billingAddress->getExtensionAttributes()->getHouseNumber();

            if ($billingAddress->getExtensionAttributes()) {
                $billingAddress->setHouseNumber($houseNumber);
            }
        }
    }
}