<?php

namespace Swissclinic\CheckoutExtraFields\Plugin\Checkout\Model;

use Swissclinic\CheckoutExtraFields\Helper\Data as Helper;

class ShippingInformationManagement
{
    private $_helper;

    public function __construct(
        Helper $helper
    )
    {
        $this->_helper = $helper;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    )
    {
        $enabled = $this->_helper->isEnabled();

        if ($enabled) {
            $shippingAddress = $addressInformation->getShippingAddress();
            $billingAddress = $addressInformation->getBillingAddress();

            if ($shippingAddress->getExtensionAttributes()) {
                $houseNumber = $shippingAddress->getExtensionAttributes()->getHouseNumber();

                $shippingAddress->setHouseNumber($houseNumber);
                $billingAddress->setHouseNumber($houseNumber);
            }
        }
    }
}