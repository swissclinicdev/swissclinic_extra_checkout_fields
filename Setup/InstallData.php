<?php

namespace Swissclinic\CheckoutExtraFields\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Customer\Setup\CustomerSetupFactory;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Quote\Setup\QuoteSetupFactory;
use Magento\Customer\Api\AddressMetadataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{

    const CUSTOM_ATTRIBUTE_CODE = 'house_number';

    private $salesSetupFactory;

    private $customerSetupFactory;

    private $quoteSetupFactory;

    public function __construct(
        \Magento\Customer\Setup\CustomerSetupFactory $customerSetupFactory,
        \Magento\Sales\Setup\SalesSetupFactory $salesSetupFactory,
        \Magento\Quote\Setup\QuoteSetupFactory $quoteSetupFactory
    )
    {
        $this->salesSetupFactory = $salesSetupFactory;
        $this->customerSetupFactory = $customerSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $eavTypes = [
            'order_address' => $this->salesSetupFactory->create(['setup' => $setup]),
            'customer_address' => $this->customerSetupFactory->create(['setup' => $setup]),
            'quote_address' => $this->quoteSetupFactory->create(['setup' => $setup])
        ];

        $attributeArray = [
            'type' => Table::TYPE_TEXT,
            'label' => 'House Number',
            'input' => 'text',
            'required' => false,
            'sort_order' => 75,
            'position' => 75
        ];

        foreach ($eavTypes as $code => $typeSetup) {
            $typeSetup->addAttribute(
                $code,
                self::CUSTOM_ATTRIBUTE_CODE,
                $attributeArray
            );
        }

        $attribute = $eavTypes['customer_address']->getEavConfig()->getAttribute(
            'customer_address',
            self::CUSTOM_ATTRIBUTE_CODE
        );

        $attribute->setData(
            'used_in_forms',
            [
                'adminhtml_customer_address',
                'customer_address_edit'
            ]
        );

        $attribute->save();
    }
}