<?php

namespace Swissclinic\CheckoutExtraFields\Helper;

use Swissclinic\CheckoutExtraFields\Model\Config;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper {

    protected $_config;

    protected $scopeConfig;

    public function __construct(
        Config $config,
        Context $context,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->_config = $config;
    }

    public function isEnabled($store = null){
        return	$this->_config->isEnabled($store);
    }
}