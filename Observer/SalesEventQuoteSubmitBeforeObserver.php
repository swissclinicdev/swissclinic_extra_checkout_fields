<?php
namespace Swissclinic\CheckoutExtraFields\Observer;

use Swissclinic\CheckoutExtraFields\Helper\Data as Helper;
use Magento\Framework\Event\ObserverInterface;

class SalesEventQuoteSubmitBeforeObserver implements ObserverInterface
{
    /*
     * Swissclinic\CheckoutExtraFields\Helper\Data $_helper
     */
    protected $_helper;

    public function __construct(Helper $helper)
    {
        $this->_helper = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();

        $shippingAddress = $observer->getQuote()->getShippingAddress();
        $houseNumber = $observer->getQuote()->getShippingAddress()->getHouseNumber();
        $enabled = $this->_helper->isEnabled();

        if ($enabled) {
            $observer->getOrder()->getShippingAddress()->setHouseNumber($quote->getShippingAddress()->getHouseNumber());
            $observer->getOrder()->getBillingAddress()->setHouseNumber($quote->getBillingAddress()->getHouseNumber());
        }

        return $this;
    }
}
